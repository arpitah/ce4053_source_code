#include <stdio.h>
#include <stdlib.h>

#include <os.h>

extern OS_MEM MyPartition;
extern CPU_INT08U MyPartitionStorage[50][100];

extern OS_ERR  err;
AVL *AVLroot=NULL;

AVL* AVLInsert(OS_TCB *p_tcb_, AVL *AVLroot)
{
  if(AVLroot==NULL)
  {
    // AVLroot=(AVL*)malloc(sizeof(AVL));
    AVLroot = (AVL *)OSMemGet((OS_MEM *)&MyPartition,(OS_ERR *)&err);
    AVLroot->p_tcb_avl = p_tcb_;
    AVLroot->left=NULL;
    AVLroot->right=NULL;
    AVLroot->next=NULL;
    AVLroot->duplicate_num=0;
  }
  else if(p_tcb_->Next_Release == AVLroot->p_tcb_avl->Next_Release)
  {
    AVL* p_node;
    //p_node=(AVL*)malloc(sizeof(AVL));
    p_node = (AVL *)OSMemGet((OS_MEM *)&MyPartition,(OS_ERR *)&err);
    p_node->p_tcb_avl = p_tcb_;
    p_node->left=NULL;
    p_node->right=NULL;
    p_node->next=NULL;
    p_node->duplicate_num=0;
    
    if(AVLroot->duplicate_num == 0)
    {
      AVLroot->next = p_node;
      (AVLroot->duplicate_num)++;
      p_node = NULL;
      return AVLroot;
    }
    else
    {
      AVL *temp = AVLroot;
      (temp->duplicate_num)++;
      while(temp->next != NULL)
      {
        temp = temp->next;
      }
      temp->next = p_node;
      p_node = NULL;
      return AVLroot;
    }
  }
  else
    if(p_tcb_->Next_Release > AVLroot->p_tcb_avl->Next_Release)        // insert in right subtree
    {
      AVLroot->right=AVLInsert(p_tcb_,AVLroot->right);
      if(BF(AVLroot)==-2)
        if(p_tcb_->Next_Release > AVLroot->right->p_tcb_avl->Next_Release)
          AVLroot=RR(AVLroot);
        else
          AVLroot=RL(AVLroot);
    }
    else
      if(p_tcb_->Next_Release<AVLroot->p_tcb_avl->Next_Release)
      {
        AVLroot->left=AVLInsert(p_tcb_,AVLroot->left);
        if(BF(AVLroot)==2)
          if(p_tcb_->Next_Release < AVLroot->left->p_tcb_avl->Next_Release)
            AVLroot=LL(AVLroot);
          else
            AVLroot=LR(AVLroot);
      }
  
  AVLroot->ht=height(AVLroot);
  
  return(AVLroot);
}

// Recursive function to delete a node with given key
// from subtree with given root. It returns root of
// the modified subtree.
AVL* AVLdeleteNode(AVL* AVLroot, OS_TCB * p_tcb_)
{
  // STEP 1: PERFORM STANDARD BST DELETE
  
  if (AVLroot == NULL)
    return AVLroot;
  
  // If the key to be deleted is smaller than the
  // root's key, then it lies in left subtree
  if ( p_tcb_->Next_Release < AVLroot->p_tcb_avl->Next_Release )
    AVLroot->left = AVLdeleteNode(AVLroot->left, p_tcb_);
  
  // If the key to be deleted is greater than the
  // root's key, then it lies in right subtree
  else if( p_tcb_->Next_Release > AVLroot->p_tcb_avl->Next_Release)
    AVLroot->right = AVLdeleteNode(AVLroot->right, p_tcb_);
  
  else if(p_tcb_->Next_Release == AVLroot->p_tcb_avl->Next_Release)
  {
    if(AVLroot->duplicate_num == 0)
    {
      {
        // node with only one child or no child
        if( (AVLroot->left == NULL) || (AVLroot->right == NULL) )
        {
          AVL *temp = AVLroot->left ? AVLroot->left :
            AVLroot->right;
            
            // No child case
            if (temp == NULL)
            {
              temp = AVLroot;
              AVLroot = NULL;
            }
            else // One child case
              *AVLroot = *temp; // Copy the contents of
            // the non-empty child
        }
        else
        {
          // node with two children: Get the inorder
          // successor (smallest in the right subtree)
          AVL* temp = minValueNode(AVLroot->right);
          
          // Copy the inorder successor's data to this node
          AVLroot->p_tcb_avl = temp->p_tcb_avl;
          
          // Delete the inorder successor
          AVLroot->right = AVLdeleteNode(AVLroot->right, temp->p_tcb_avl);
        }
      }
    }
    // else if node to be deleted is the first node, but duplicates are present
    else if(AVLroot->duplicate_num > 0)
    {
      {//if first node is the ptcb we r looking for
        if(AVLroot->p_tcb_avl == p_tcb_)
        {
          AVL *temp = AVLroot;
          AVLroot->p_tcb_avl = p_tcb_;
          AVLroot->next = temp->next->next;
          (AVLroot->duplicate_num)--;
          OSMemPut((OS_MEM *)&MyPartition,(AVL *)temp,(OS_ERR *)&err);
          return AVLroot;
        }
        //else search node in between the list
        else
        {
          AVL *temp,*prev;
          temp=AVLroot;
          (AVLroot->duplicate_num)--;
          while(temp->p_tcb_avl != p_tcb_)
          {
            prev = temp;
            temp = temp->next;
          }
          prev->next = temp->next;
          OSMemPut((OS_MEM *)&MyPartition,(AVL *)temp,(OS_ERR *)&err);
          return AVLroot;
        }
      }
   /*   AVL* temp = AVLroot->next;
      AVLroot->duplicate_num = AVLroot->duplicate_num - 1;
      AVLroot->next = temp->next;
      //free(temp);
      OSMemPut((OS_MEM *)&MyPartition,(AVL *)temp,(OS_ERR *)&err);
      return AVLroot;*/
    }
  }
  
  // if key is same as root's key, then This is
  // the node to be deleted
  else
  {
    // node with only one child or no child
    if( (AVLroot->left == NULL) || (AVLroot->right == NULL) )
    {
      AVL *temp = AVLroot->left ? AVLroot->left :
        AVLroot->right;
        
        // No child case
        if (temp == NULL)
        {
          temp = AVLroot;
          AVLroot = NULL;
        }
        else // One child case
          *AVLroot = *temp; // Copy the contents of
        // the non-empty child
    }
    else
    {
      // node with two children: Get the inorder
      // successor (smallest in the right subtree)
      AVL* temp = minValueNode(AVLroot->right);
      
      // Copy the inorder successor's data to this node
      AVLroot->p_tcb_avl = temp->p_tcb_avl;
      
      // Delete the inorder successor
      AVLroot->right = AVLdeleteNode(AVLroot->right, temp->p_tcb_avl);
    }
  }
  
  // If the tree had only one node then return
  if (AVLroot == NULL)
    return AVLroot;
  
  // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
  AVLroot->ht = 1 + max(height(AVLroot->left),
                        height(AVLroot->right));
  
  // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to
  // check whether this node became unbalanced)
  int balance = BF(AVLroot);
  
  // If this node becomes unbalanced, then there are 4 cases
  
  // Left Left Case
  if (balance > 1 && BF(AVLroot->left) >= 0)
    return (rotateright(AVLroot));
  
  // Left Right Case
  if (balance > 1 && BF(AVLroot->left) < 0)
  {
    AVLroot->left =  rotateleft(AVLroot->left);
    return rotateright(AVLroot);
  }
  
  // Right Right Case
  if (balance < -1 && BF(AVLroot->right) <= 0)
    return rotateleft(AVLroot);
  
  // Right Left Case
  if (balance < -1 && BF(AVLroot->right) > 0)
  {
    AVLroot->right = rotateright(AVLroot->right);
    return rotateleft(AVLroot);
  }
  return AVLroot;
}

int height(AVL *T)
{
  int lh,rh;
  if(T==NULL)
    return(0);
  
  if(T->left==NULL)
    lh=0;
  else
    lh=1+T->left->ht;
  
  if(T->right==NULL)
    rh=0;
  else
    rh=1+T->right->ht;
  
  if(lh>rh)
    return(lh);
  
  return(rh);
}

AVL* rotateright(AVL *x)
{
  AVL *y;
  y=x->left;
  x->left=y->right;
  y->right=x;
  x->ht=height(x);
  y->ht=height(y);
  return(y);
}

AVL* rotateleft(AVL *x)
{
  AVL *y;
  y=x->right;
  x->right=y->left;
  y->left=x;
  x->ht=height(x);
  y->ht=height(y);
  
  return(y);
}

AVL * RR(AVL *T)
{
  T=rotateleft(T);
  return(T);
}

AVL * LL(AVL *T)
{
  T=rotateright(T);
  return(T);
}

AVL * LR(AVL *T)
{
  T->left=rotateleft(T->left);
  T=rotateright(T);
  
  return(T);
}

AVL * RL(AVL *T)
{
  T->right=rotateright(T->right);
  T=rotateleft(T);
  return(T);
}

int BF(AVL *T)
{
  int lh,rh;
  if(T==NULL)
    return(0);
  
  if(T->left==NULL)
    lh=0;
  else
    lh=1+T->left->ht;
  
  if(T->right==NULL)
    rh=0;
  else
    rh=1+T->right->ht;
  
  return(lh-rh);
}

// A utility function to get maximum of two integers
int max(int a, int b)
{
  return (a > b)? a : b;
}

/* Given a non-empty binary search tree, return the
node with minimum key value found in that tree.
Note that the entire tree does not need to be
searched. */
AVL * minValueNode(AVL* node)
{
  AVL* current = node;
  if(current == NULL)
  {
    return NULL;
  }
  /* loop down to find the leftmost leaf */
  while (current->left != NULL)
    current = current->left;
  
  return current;
}
