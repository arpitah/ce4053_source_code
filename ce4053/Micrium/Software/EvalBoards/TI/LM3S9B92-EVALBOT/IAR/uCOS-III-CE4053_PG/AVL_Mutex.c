#include <stdio.h>
//#include<malloc.h>
#include <stdlib.h>
#include <os.h>
extern OS_MEM MyPartition;
extern CPU_INT08U MyPartitionStorage[50][100];

extern OS_ERR  err;
/*typedef struct AVL_mutex
{
mutex *p_mutex_avl;
struct AVL_mutex *left,*right;
int ht;
}AVL_mutex;

AVL_mutex *AVL_insert_mutex(mutex* , AVL_mutex*);
void preorder_mutex(AVL_mutex *);
void inorder_mutex(AVL_mutex *);
int height_mutex( AVL_mutex *);
AVL_mutex *rotateright_mutex(AVL_mutex *) ;
AVL_mutex *rotateleft_mutex(AVL_mutex *);
AVL_mutex *RR_mutex(AVL_mutex *);
AVL_mutex *LL_mutex(AVL_mutex *);
AVL_mutex *LR_mutex(AVL_mutex *);
AVL_mutex *RL_mutex(AVL_mutex *);
int BF_mutex(AVL_mutex *);
int max_mutex(int , int );
AVL_mutex *minValueNode_mutex(AVL_mutex *);
AVL_mutex *deleteNode_mutex(AVL_mutex *, mutex* );
AVL_mutex *min_system_ceil;
*/
AVL_mutex *AVLtroot=NULL;

OS_TICK System_ceil = 1000000;

AVL_mutex* AVL_insert_mutex(mutex *p_mutex, AVL_mutex *AVLtroot)
{
  if(AVLtroot==NULL)
  {
    //AVLtroot=(AVL*)malloc(sizeof(AVL));
    AVLtroot = (AVL_mutex *)OSMemGet((OS_MEM *)&MyPartition,(OS_ERR *)&err);
    AVLtroot->p_mutex_avl = p_mutex;
    AVLtroot->left=NULL;
    AVLtroot->right=NULL;
    AVLtroot->count = 0;
  }
  else
    //if(p_mutex->mutex > AVLtroot->p_mutex_avl->Next_Release)        // insert in right subtree
    if(p_mutex->Resource_ceil > AVLtroot->p_mutex_avl->Resource_ceil)
    {
      AVLtroot->right=AVL_insert_mutex(p_mutex,AVLtroot->right);
      if(BF_mutex(AVLtroot)==-2)
        if(p_mutex->Resource_ceil > AVLtroot->right->p_mutex_avl->Resource_ceil)
          AVLtroot=RR_mutex(AVLtroot);
        else
          AVLtroot=RL_mutex(AVLtroot);
    }
    else
      if(p_mutex->Resource_ceil < AVLtroot->p_mutex_avl->Resource_ceil)
      {
        //AVLtroot->left=AVL_insert(p_mutex>Next_Release,AVLroot->left);
        AVLtroot->left = AVL_insert_mutex(p_mutex,AVLtroot->left);
        if(BF_mutex(AVLtroot)==2)
          // if(p_tcb_->Next_Release < AVLtroot->left->p_tcb_avl->Next_Release)
          if(p_mutex->Resource_ceil < AVLtroot->left->p_mutex_avl->Resource_ceil)
            AVLtroot=LL_mutex(AVLtroot);
          else
            AVLtroot=LR_mutex(AVLtroot);
      }
    else if(p_mutex->Resource_ceil == AVLtroot->p_mutex_avl->Resource_ceil)
    {
      AVLtroot->count++;
    }
  
  AVLtroot->ht=height_mutex(AVLtroot);
  
  return(AVLtroot);
}

// Recursive function to delete a node with given key
// from subtree with given root. It returns root of
// the modified subtree.
AVL_mutex* deleteNode_mutex(AVL_mutex* AVLtroot, mutex* p_mutex)
{
  // STEP 1: PERFORM STANDARD BST DELETE
  
  if (AVLtroot == NULL)
    return AVLtroot;
  
  // If the key to be deleted is smaller than the
  // root's key, then it lies in left subtree
  //if ( next_release < AVLroot->p_tcb_avl->Next_Release )
  if ( p_mutex->Resource_ceil < AVLtroot->p_mutex_avl->Resource_ceil)
    AVLtroot->left = deleteNode_mutex(AVLtroot->left, p_mutex);
  
  // If the key to be deleted is greater than the
  // root's key, then it lies in right subtree
  //else if( next_release > AVLroot->p_tcb_avl->Next_Release)
  else if( p_mutex->Resource_ceil > AVLtroot->p_mutex_avl->Resource_ceil)
    AVLtroot->right = deleteNode_mutex(AVLtroot->right,p_mutex);
  
  // if key is same as root's key, then This is
  // the node to be deleted
  else
  {
    if(AVLtroot->count > 0)
  {
    AVLtroot->count--;
  return AVLtroot;
}
    // node with only one child or no child
    if( (AVLtroot->left == NULL) || (AVLtroot->right == NULL) )
    {
      AVL_mutex *temp = AVLtroot->left ? AVLtroot->left :
        AVLtroot->right;
        
        // No child case
        if (temp == NULL)
        {
          temp = AVLtroot;
          AVLtroot = NULL;
        }
        else // One child case
          *AVLtroot = *temp; // Copy the contents of
        // the non-empty child
        //free(temp);
        OSMemPut((OS_MEM *)&MyPartition,(AVL_mutex *)temp,(OS_ERR *)&err);
    }
    else
    {
      // node with two children: Get the inorder
      // successor (smallest in the right subtree)
      AVL_mutex* temp = minValueNode_mutex(AVLtroot->right);
      
      // Copy the inorder successor's data to this node
      AVLtroot->p_mutex_avl = temp->p_mutex_avl;
      
      // Delete the inorder successor
      AVLtroot->right = deleteNode_mutex(AVLtroot->right, temp->p_mutex_avl);
    }
  }
  
  // If the tree had only one node then return
  if (AVLtroot == NULL)
    return AVLtroot;
  
  // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
  AVLtroot->ht = 1 + max_mutex(height_mutex(AVLtroot->left),
                               height_mutex(AVLtroot->right));
  
  // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to
  // check whether this node became unbalanced)
  int balance = BF_mutex(AVLtroot);
  
  // If this node becomes unbalanced, then there are 4 cases
  
  // Left Left Case
  if (balance > 1 && BF_mutex(AVLtroot->left) >= 0)
    return rotateright_mutex(AVLtroot);
  
  // Left Right Case
  if (balance > 1 && BF_mutex(AVLtroot->left) < 0)
  {
    AVLtroot->left =  rotateleft_mutex(AVLtroot->left);
    return rotateright_mutex(AVLtroot);
  }
  
  // Right Right Case
  if (balance < -1 && BF_mutex(AVLtroot->right) <= 0)
    return rotateleft_mutex(AVLtroot);
  
  // Right Left Case
  if (balance < -1 && BF_mutex(AVLtroot->right) > 0)
  {
    AVLtroot->right = rotateright_mutex(AVLtroot->right);
    return rotateleft_mutex(AVLtroot);
  }
  
  return AVLtroot;
}

int height_mutex(AVL_mutex *T)
{
  int lh,rh;
  if(T==NULL)
    return(0);
  
  if(T->left==NULL)
    lh=0;
  else
    lh=1+T->left->ht;
  
  if(T->right==NULL)
    rh=0;
  else
    rh=1+T->right->ht;
  
  if(lh>rh)
    return(lh);
  
  return(rh);
}

AVL_mutex * rotateright_mutex(AVL_mutex *x)
{
  AVL_mutex *y;
  y=x->left;
  x->left=y->right;
  y->right=x;
  x->ht=height_mutex(x);
  y->ht=height_mutex(y);
  return(y);
}

AVL_mutex * rotateleft_mutex(AVL_mutex *x)
{
  AVL_mutex *y;
  y=x->right;
  x->right=y->left;
  y->left=x;
  x->ht=height_mutex(x);
  y->ht=height_mutex(y);
  
  return(y);
}

AVL_mutex * RR_mutex(AVL_mutex *T)
{
  T=rotateleft_mutex(T);
  return(T);
}

AVL_mutex * LL_mutex(AVL_mutex *T)
{
  T=rotateright_mutex(T);
  return(T);
}

AVL_mutex * LR_mutex(AVL_mutex *T)
{
  T->left=rotateleft_mutex(T->left);
  T=rotateright_mutex(T);
  
  return(T);
}

AVL_mutex * RL_mutex(AVL_mutex *T)
{
  T->right=rotateright_mutex(T->right);
  T=rotateleft_mutex(T);
  return(T);
}

int BF_mutex(AVL_mutex *T)
{
  int lh,rh;
  if(T==NULL)
    return(0);
  
  if(T->left==NULL)
    lh=0;
  else
    lh=1+T->left->ht;
  
  if(T->right==NULL)
    rh=0;
  else
    rh=1+T->right->ht;
  
  return(lh-rh);
}


// A utility function to get maximum of two integers
int max_mutex(int a, int b)
{
  return (a > b)? a : b;
}

/* Given a non-empty binary search tree, return the
node with minimum key value found in that tree.
Note that the entire tree does not need to be
searched. */
AVL_mutex * minValueNode_mutex(AVL_mutex* node)
{
  AVL_mutex* current = node; 
  
  /* loop down to find the leftmost leaf */
  while (current->left != NULL)
    current = current->left;
  
  return current;
}



void min_system_ceil(AVL_mutex* AVLtroot)
{
  AVL_mutex *current;
  if(AVLtroot == NULL)
  {
    System_ceil = 1000000;
  }
  else
  {
    current = AVLtroot;
    if(current->left == NULL)
    {
      System_ceil = current->p_mutex_avl->Resource_ceil;
    }
    else
    {
      while (current->left != NULL)
      {
        current = current->left;
      }
      System_ceil = current->p_mutex_avl->Resource_ceil;
    }
  }
}











