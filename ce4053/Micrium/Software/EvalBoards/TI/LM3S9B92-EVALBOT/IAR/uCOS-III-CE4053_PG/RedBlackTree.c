#include <os.h>
#include <math.h>
#include <stdio.h>
#include "splay.h"
#define print_splay 1
extern OS_MEM MyPartition;
extern CPU_INT08U MyPartitionStorage[50][100];

OS_TCB* Sched_min_p_tcb;
OS_TCB* ptr_rdylist;

SchedSplay* root_sched=NULL;
extern AVL *AVLroot;
extern OS_ERR  err;
/*********************************Splay tree*******************************************/
/* RR(Y rotates to the right):
k2                   k1
/  \                 /  \
k1   Z     ==>       X   k2
/ \                      /  \
X   Y                    Y    Z
*/

SchedSplay* SRR_Rotate(SchedSplay* k2)
{
  SchedSplay* k1 = k2->lchild;
  k2->lchild = k1->rchild;
  k1->rchild = k2;
  return k1;
}

/* LL(Y rotates to the left):
k2                       k1
/  \                     /  \
X    k1         ==>      k2   Z
/  \                /  \
Y    Z              X    Y
*/
SchedSplay* SLL_Rotate(SchedSplay* k2)
{
  SchedSplay* k1 = k2->rchild;
  k2->rchild = k1->lchild;
  k1->lchild = k2;
  return k1;
}

/* An implementation of top-down splay tree
If key is in the tree, then the node containing the key will be rotated to root_sched,
 else the last non-NULL node (on the search path) will be rotated to root_sched.
*/
SchedSplay* SSplay(OS_TICK next_release, SchedSplay* root_sched)
{
  if(!root_sched)
    return root_sched;
  //return NULL;
  SchedSplay header;
  /* header.rchild points to L tree; header.lchild points to R Tree */
  header.lchild = header.rchild = NULL;
  SchedSplay* LeftTreeMax = &header;
  SchedSplay* RightTreeMin = &header;
  
  /* loop until root_sched_sched->lchild == NULL || root_sched_sched->rchild == NULL; then break!
  (or when find the key, break too.)
  The zig/zag mode would only happen when cannot find key and will reach
  null on one side after RR or LL Rotation.
  */
  while(1)
  {
    if(next_release < root_sched->p_tcb_splay->Next_Release)
    {
      if(!root_sched->lchild)
        break;
      if(next_release < root_sched->lchild->p_tcb_splay->Next_Release)
      {
        root_sched = SRR_Rotate(root_sched); /* only zig-zig mode need to rotate once,
        because zig-zag mode is handled as zig
        mode, which doesn't require rotate,
        just linking it to R Tree */
        if(!root_sched->lchild)
          break;
      }
      /* Link to R Tree */
      RightTreeMin->lchild = root_sched;
      RightTreeMin = RightTreeMin->lchild;
      root_sched = root_sched->lchild;
      RightTreeMin->lchild = NULL;
    }
    else if(next_release > root_sched->p_tcb_splay->Next_Release)
    {
      if(!root_sched->rchild)
        break;
      if(next_release > root_sched->rchild->p_tcb_splay->Next_Release)
      {
        root_sched = SLL_Rotate(root_sched);/* only zag-zag mode need to rotate once,
        because zag-zig mode is handled as zag
        mode, which doesn't require rotate,
        just linking it to L Tree */
        if(!root_sched->rchild)
          break;
      }
      /* Link to L Tree */
      LeftTreeMax->rchild = root_sched;
      LeftTreeMax = LeftTreeMax->rchild;
      root_sched = root_sched->rchild;
      LeftTreeMax->rchild = NULL;
    }
    else
      break;
  }
  /* assemble L Tree, Middle Tree and R tree together */
  LeftTreeMax->rchild = root_sched->lchild;
  RightTreeMin->lchild = root_sched->rchild;
  root_sched->lchild = header.rchild;
  root_sched->rchild = header.lchild;
  
  return root_sched;
}

SchedSplay* SNew_Node(OS_TCB *p_tcb_)
{
  
  //SchedSplay* p_node = new splay;
  SchedSplay *p_node = (SchedSplay *)OSMemGet((OS_MEM *)&MyPartition,(OS_ERR *)&err);
  //SchedSplay *p_node = (SchedSplay *)malloc(sizeof(SchedSplay));
#ifdef print_splay
  printf("\nNew Memory Allocated");
#endif
  p_node->p_tcb_splay = p_tcb_;
  p_node->lchild = p_node->rchild = NULL;
  return p_node;
}

/* Implementation 1:
First Splay(key, root_sched)(and assume the tree we get is called *), so root_sched node and
its left child tree will contain nodes with keys <= key, so we could rebuild
the tree, using the newly alloced node as a root_sched, the children of original tree
*(including root_sched node of *) as this new node's children.
NOTE: This implementation is much better! Reasons are as follows in implementation 2.
NOTE: This implementation of splay tree doesn't allow nodes of duplicate keys!
*/
SchedSplay* SInsert(OS_TCB *p_tcb_, SchedSplay* root_sched)
{
  //static SchedSplay* p_node = NULL;
  SchedSplay* p_node = NULL;
  /* if(!p_node){
  p_node = SNew_Node(p_tcb_);
}
        else // could take advantage of the node remains because of there was duplicate key before.     
  p_node->p_tcb_splay = p_tcb_;*/
  
  p_node = (SchedSplay *)OSMemGet((OS_MEM *)&MyPartition,(OS_ERR *)&err);
  
#ifdef print_splay
  printf("\nNew Memory Allocated");
#endif
  p_node->p_tcb_splay = p_tcb_;
  p_node->lchild = p_node->rchild = NULL;
  
  /***************initialize the stack*************/
//  p_node->p_tcb_splay->StkPtr = OSTaskStkInit(p_node->p_tcb_splay->TaskEntryAddr, p_node->p_tcb_splay->TaskEntryArg, p_node->p_tcb_splay->StkBasePtr,
//                                              p_node->p_tcb_splay->StkLimitPtr, p_node->p_tcb_splay->StkSize, p_node->p_tcb_splay->Opt);
  
  
  if(!root_sched)
  {
#ifdef print_splay
    printf("\nFirst root_sched...");
#endif
    root_sched = p_node;
    p_node = NULL;
    
    Sched_min_p_tcb = root_sched->p_tcb_splay;
    return root_sched;
  }
  root_sched = SSplay(p_tcb_->Next_Release, root_sched);
  if(p_tcb_->Next_Release == root_sched->p_tcb_splay->Next_Release)   //duplicate values
  {
    if(root_sched->duplicate_nbr == 0)
    {
      root_sched->next = p_node;
      root_sched->duplicate_nbr++;
      p_node = NULL;
      return root_sched;
    }
    else
    {
      SchedSplay *temp = root_sched;
      (temp->duplicate_nbr)++;
      while(temp->next != NULL)
      {
        temp = temp->next;
      }
      temp->next = p_node;
      p_node = NULL;
      return root_sched;
    }
  }
  /* This is BST that, all keys <= root_sched->key is in root_sched->lchild, all keys >
  root_sched->key is in root_sched->rchild. (This BST doesn't allow duplicate keys.) NOW IT DOES...!!! */
  if(p_tcb_->Next_Release < root_sched->p_tcb_splay->Next_Release)
  {
    p_node->lchild = root_sched->lchild;
    p_node->rchild = root_sched;
    root_sched->lchild = NULL;
    root_sched = p_node;
    
    Sched_min_p_tcb = root_sched->p_tcb_splay;  //points to min p_tcb
    
#ifdef print_splay
    printf("\n root_sched left 1...");
#endif
  }
  else if(p_tcb_->Next_Release > root_sched->p_tcb_splay->Next_Release)
  {
    p_node->rchild = root_sched->rchild;
    p_node->lchild = root_sched;
    root_sched->rchild = NULL;
    root_sched = p_node;
#ifdef print_splay
    printf("\n root_sched right 1...");
#endif
  }
  else
    return root_sched;
  p_node = NULL;
  return root_sched;
}

/****************************************DELETE TASK**********************************************/
SchedSplay* SDelete(OS_TCB *p_tcb_, SchedSplay* root_sched)
{
  SchedSplay* temp,* temp1;
  SchedSplay* prev_temp;
  if(!root_sched)
    return root_sched;
  //return NULL;
  root_sched = SSplay(p_tcb_->Next_Release, root_sched);

  //point to min
  if(root_sched->rchild == NULL)
  {
    Sched_min_p_tcb = NULL;
  }
  else
  {
    temp1 = root_sched->rchild;
    while(temp1->lchild != NULL)
    {
      temp1 = temp1->lchild;
    }
    Sched_min_p_tcb = temp1->p_tcb_splay;
  }
  //END point to min
  
  if(p_tcb_->Next_Release != root_sched->p_tcb_splay->Next_Release) // No such node in splay tree
    return root_sched;
  // Node with this Next_Release value found, now have to search for a particular TCB
  else
  {
    // node to be deleted is the only node
    if((root_sched->p_tcb_splay == p_tcb_) &&(root_sched->duplicate_nbr == 0))
    {
      if(!root_sched->lchild)
      {
        temp = root_sched;
        root_sched = root_sched->rchild;
      }
      else
      {
        temp = root_sched;
        /*Note: Since key == root_sched->key, so after Splay(key, root_sched->lchild),
        the tree we get will have no right child tree. (key > any key in
        oot->lchild)*/
        root_sched = Splay(p_tcb_->Next_Release, root_sched->lchild);
        root_sched->rchild = temp->rchild;
      }
      OSMemPut((OS_MEM *)&MyPartition,(SchedSplay *)temp,(OS_ERR *)&err);
      //free(temp);
      return root_sched;
    }
    
    // else if node to be deleted is the first node, but duplicates are present
    else if((root_sched->p_tcb_splay == p_tcb_) &&(root_sched->duplicate_nbr > 0))
    {
      temp = root_sched->next;
      root_sched->duplicate_nbr = root_sched->duplicate_nbr - 1;
      root_sched->next = temp->next;
      root_sched->p_tcb_splay = temp->p_tcb_splay;
      OSMemPut((OS_MEM *)&MyPartition,(SchedSplay *)temp,(OS_ERR *)&err);
      return root_sched;
    }
    
    //else if node to be deleted in inside linked list, have to traverse till there
    //and then delete
    else if((root_sched->duplicate_nbr > 0) && (root_sched->p_tcb_splay != p_tcb_))
    {
      temp = root_sched;
      while(temp->p_tcb_splay != p_tcb_)
      {
        prev_temp = temp;
        temp = temp->next;
      }
      root_sched->duplicate_nbr = root_sched->duplicate_nbr - 1;
      prev_temp->next = temp->next;
      OSMemPut((OS_MEM *)&MyPartition,(SchedSplay *)temp,(OS_ERR *)&err);
      return root_sched;
    }
  }
}              


/**************************************Search Minimum************************************************************************/
OS_TCB* searchMin(void)
{
  /*SchedSplay *temp = root_sched;
  if(temp == NULL)
  {
    return NULL;        //check!!! returning NULL value
  }
  while(temp->link[0] != NULL)
  {
    temp = temp->link[0];
  }
  return temp->p_tcb_splay;*/
  return Sched_min_p_tcb;
}

/********************************INSERT INTO READY LIST********************************************/
void insertintoreadylist(OS_TCB* rbt_min)
{
 // OS_TCB* rbt_min = searchMin();    //searching the minimum value after inserting into RBT
  
//  if(rbt_min == NULL)
//  {
 // }
  if(ptr_rdylist == NULL)
  {
    if((rbt_min != NULL) && (rbt_min -> Period < System_ceil) )
    {
      /*OS_TaskRdy(rbt_min); 
      ptr_rdylist = rbt_min;
      OSTaskQty++;
      root_sched = SDelete(rbt_min, root_sched);*/
      EDFscheduler();
    }
    else 
    {
      printf("\nTask blocked");
      AVLroot = AVLInsert(rbt_min, AVLroot);      // insert into blocked AVL data structure
      root_sched = SDelete(rbt_min, root_sched);
    }
  }
  
  else if((rbt_min -> Next_Release < ptr_rdylist -> Next_Release) && (rbt_min -> Period < System_ceil))  //swap and pendlist??or sched data structure??
  {    
    CPU_SR_ALLOC();
    OS_CRITICAL_ENTER();
    OS_RdyListRemove(ptr_rdylist);      // remove from ready list and put into blocked avl tree 
    
    printf("\nrunning task preempted and added to edf data structure");
    
 //   AVLroot = AVLInsert(ptr_rdylist, AVLroot);      // insert into blocked AVL data structure
    root_sched = SInsert(ptr_rdylist, root_sched);
    OS_TaskRdy(rbt_min); 
    ptr_rdylist = rbt_min;
    //  OSTaskQty++;
    root_sched = SDelete(rbt_min, root_sched);
    OS_CRITICAL_EXIT_NO_SCHED();
    return;
  }
  
  else if((rbt_min -> Next_Release < ptr_rdylist -> Next_Release) && (rbt_min -> Period >= System_ceil)) // blocked list
  {
    CPU_SR_ALLOC();
    OS_CRITICAL_ENTER();
    AVLroot = AVLInsert(rbt_min, AVLroot);      //insert into blocked data structure
    root_sched = SDelete(rbt_min, root_sched);  //delete rbt_min from redblack tree
    OS_CRITICAL_EXIT_NO_SCHED();
    return;
  }
  
  else if(rbt_min -> Next_Release > ptr_rdylist -> Next_Release) // EDF sched-low priority do nothing
  {
    return;
  }
}

// task finished completion check blocked list, if no task then schedule from schedular data structure
void deletefromreadylist(void)
{
  if(AVLroot == NULL)           //blocked list is empty
  {                                             // do normal edf scheduling
   /* OS_TCB* rbt_min = searchMin();
    if(rbt_min == NULL)
    {
      ptr_rdylist = NULL;
    }
    else 
    {
      OS_TaskRdy(rbt_min); 
      ptr_rdylist = rbt_min;
      OSTaskQty++;
      root_sched = SDelete(rbt_min, root_sched);
    }*/
    EDFscheduler();
  }
  else  //blocked list not empty, hence take the next highest priority tcb and schedule that
  {
    AVL* temp = minValueNode(AVLroot);
    CPU_SR_ALLOC();
    OS_CRITICAL_ENTER();
    OS_TaskRdy(temp->p_tcb_avl);
    //root_sched = SInsert(temp_tcb, root_sched);    //inserting into redblack tree with next release value updated

    ptr_rdylist = temp->p_tcb_avl;
    
    AVLroot = AVLdeleteNode(AVLroot, temp->p_tcb_avl);
    OS_CRITICAL_EXIT_NO_SCHED(); 
  }
}

/***************** mutex is released, deleted from the ALV_mutex tree    *******************/
void search_least_deadline(void)
{
  AVL* temp = minValueNode(AVLroot);
  if(temp == NULL)      //No blocked tasks
  {
    //do nothing, let the running task complete
  }
  else if((temp->p_tcb_avl->Period < System_ceil)&&((temp->p_tcb_avl->Next_Release < ptr_rdylist->Next_Release)))     //preemt - find next high priority task 
  {
    CPU_SR_ALLOC();
    OS_CRITICAL_ENTER();
    OS_RdyListRemove(ptr_rdylist);      // remove from ready list and put into blocked avl tree       
    //AVLroot = AVLInsert(ptr_rdylist, AVLroot);      // insert into blocked AVL data structure
    root_sched = SInsert(ptr_rdylist, root_sched);
    OS_TaskRdy(temp->p_tcb_avl); 
    ptr_rdylist = temp->p_tcb_avl;
    //  OSTaskQty++;
    
    AVLroot = AVLdeleteNode(AVLroot, temp->p_tcb_avl);
    OS_CRITICAL_EXIT_NO_SCHED();
    return;
  }
  // else do nothin, let the task complete execution
}

void EDFscheduler(void)
{
  CPU_SR_ALLOC();
  OS_CRITICAL_ENTER(); 
  OS_TCB* rbt_min = searchMin();    //searching the minimum value after inserting into RBT
  if(rbt_min == NULL)
  {
  }
  else if(ptr_rdylist == NULL)
  {
    if(rbt_min != NULL)
    {
      OS_TaskRdy(rbt_min); 
      ptr_rdylist = rbt_min;
      OSTaskQty++;
      root_sched = SDelete(rbt_min, root_sched);
    }
  }
  OS_CRITICAL_EXIT_NO_SCHED();
  OSSched();
}
